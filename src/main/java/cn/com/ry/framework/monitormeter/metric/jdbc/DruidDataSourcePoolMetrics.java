package cn.com.ry.framework.monitormeter.metric.jdbc;

import cn.com.ry.framework.monitormeter.instrument.linklog.LinkLogMeterRegistry;
import com.alibaba.druid.pool.DruidDataSource;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.binder.MeterBinder;
import io.micrometer.prometheus.PrometheusMeterRegistry;

import javax.sql.DataSource;

public class DruidDataSourcePoolMetrics implements MeterBinder {

    //Bean 名称
    private String name;
    private DruidDataSource druidDataSource;
    private Iterable<Tag> tags;

    public DruidDataSourcePoolMetrics(String name, DataSource dataSource, Iterable<Tag> tags) {
        if (dataSource != null && dataSource instanceof DruidDataSource) {
            this.druidDataSource = (DruidDataSource) dataSource;
        }
        this.name = name;
        this.tags = tags;
    }

    public DruidDataSourcePoolMetrics(String name, DataSource dataSource) {
        if (dataSource != null && dataSource instanceof DruidDataSource) {
            this.druidDataSource = (DruidDataSource) dataSource;
        }
        this.name = name;
        this.tags = Tags.of("linklog.jdbc.type", "druid");
    }

    @Override
    public void bindTo(MeterRegistry registry) {
        if (druidDataSource != null && registry != null) {
            if(registry instanceof LinkLogMeterRegistry){
                Gauge.builder("jdbc.connections.druid." + name + ".active", druidDataSource, DruidDataSource::getActiveCount)
                        .tags(tags)
                        .description("druid监控，当前占用连接数")
                        .register(registry);
                Gauge.builder("jdbc.connections.druid." + name + ".max", druidDataSource, DruidDataSource::getMaxActive)
                        .tags(tags)
                        .description("druid监控，最多连接池数量")
                        .register(registry);
                Gauge.builder("jdbc.connections.druid." + name + ".min", druidDataSource, DruidDataSource::getMinIdle)
                        .tags(tags)
                        .description("druid监控，初始连接数")
                        .register(registry);
            }else if(registry instanceof PrometheusMeterRegistry){
                Gauge.builder("jdbc.connections.active", druidDataSource, DruidDataSource::getActiveCount)
                        .tags(Tags.of("datasource.name",name))
                        .description("druid监控，当前占用连接数")
                        .register(registry);
                Gauge.builder("jdbc.connections.max", druidDataSource, DruidDataSource::getMaxActive)
                        .tags(Tags.of("datasource.name",name))
                        .description("druid监控，最多连接池数量")
                        .register(registry);
                Gauge.builder("jdbc.connections.min", druidDataSource, DruidDataSource::getMinIdle)
                        .tags(Tags.of("datasource.name",name))
                        .description("druid监控，初始连接数")
                        .register(registry);
            }

        }
    }

}
