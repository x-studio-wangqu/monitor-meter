package cn.com.ry.framework.monitormeter.metric.mq;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class MqConsumerBean {

    private String topic;
    private String consumerGroup;

}
