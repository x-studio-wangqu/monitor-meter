package cn.com.ry.framework.monitormeter.metric.jdbc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class JDBCBean {
    private String name;
    private String sql;
}
