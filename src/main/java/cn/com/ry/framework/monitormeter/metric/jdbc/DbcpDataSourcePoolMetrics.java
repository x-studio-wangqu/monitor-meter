package cn.com.ry.framework.monitormeter.metric.jdbc;

import cn.com.ry.framework.monitormeter.instrument.linklog.LinkLogMeterRegistry;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.binder.MeterBinder;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;

public class DbcpDataSourcePoolMetrics implements MeterBinder {

    private String name;
    private BasicDataSource basicDataSource;
    private Iterable<Tag> tags;

    public DbcpDataSourcePoolMetrics(String name, DataSource dataSource, Iterable<Tag> tags) {
        if (dataSource != null && dataSource instanceof BasicDataSource) {
            this.basicDataSource = (BasicDataSource) dataSource;
        }
        this.name = name;
        this.tags = tags;
    }

    public DbcpDataSourcePoolMetrics(String name, DataSource dataSource) {
        if (dataSource != null && dataSource instanceof BasicDataSource) {
            this.basicDataSource = (BasicDataSource) dataSource;
        }
        this.name = name;
        this.tags = Tags.of("linklog.jdbc.type", "dbcp");
    }

    @Override
    public void bindTo(MeterRegistry registry) {
        if (basicDataSource != null && registry != null) {
            if(registry instanceof LinkLogMeterRegistry){
                Gauge.builder("jdbc.connections.dbcp." + name + ".active", basicDataSource, BasicDataSource::getNumActive)
                        .tags(tags)
                        .description("dbcp监控，当前占用连接数")
                        .register(registry);
                Gauge.builder("jdbc.connections.dbcp." + name + ".max", basicDataSource, BasicDataSource::getMaxActive)
                        .tags(tags)
                        .description("dbcp监控，最多连接池数量")
                        .register(registry);
                Gauge.builder("jdbc.connections.dbcp." + name + ".min", basicDataSource, BasicDataSource::getMinIdle)
                        .tags(tags)
                        .description("dbcp监控，初始连接数")
                        .register(registry);
            }else if(registry instanceof PrometheusMeterRegistry){
                Gauge.builder("jdbc.connections.active", basicDataSource, BasicDataSource::getNumActive)
                        .tags(Tags.of("datasource.name",name))
                        .description("dbcp监控，当前占用连接数")
                        .register(registry);
                Gauge.builder("jdbc.connections.max", basicDataSource, BasicDataSource::getMaxActive)
                        .tags(Tags.of("datasource.name",name))
                        .description("dbcp监控，最多连接池数量")
                        .register(registry);
                Gauge.builder("jdbc.connections.min", basicDataSource, BasicDataSource::getMinIdle)
                        .tags(Tags.of("datasource.name",name))
                        .description("dbcp监控，初始连接数")
                        .register(registry);
            }

        }
    }

}
