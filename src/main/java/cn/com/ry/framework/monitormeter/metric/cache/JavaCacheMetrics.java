package cn.com.ry.framework.monitormeter.metric.cache;

import cn.com.ry.framework.monitormeter.instrument.linklog.LinkLogMeterRegistry;
import cn.com.ry.framework.monitormeter.util.Assert;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.binder.MeterBinder;
import io.micrometer.prometheus.PrometheusMeterRegistry;

import java.util.List;
import java.util.Map;
import java.util.function.ToDoubleFunction;


public class JavaCacheMetrics implements MeterBinder {

    private Map map;
    private List list;
    private Double value;
    private String name;

    public JavaCacheMetrics(Map map, String name) {
        Assert.notNull(map, "ConnectionFactory must not be null");
        this.name = name;
        this.map = map;
    }

    public JavaCacheMetrics(List list, String name) {
        Assert.notNull(list, "ConnectionFactory must not be null");
        this.name = name;
        this.list = list;
    }

    public JavaCacheMetrics(Double value, String name) {
        Assert.notNull(value, "ConnectionFactory must not be null");
        this.name = name;
        this.value = value;
    }

    @Override
    public void bindTo(MeterRegistry registry) {
        if (registry != null) {
            if (registry instanceof LinkLogMeterRegistry) {
                if (value != null) {
                    Gauge.builder(name + ".double.size", value, new ToDoubleFunction<Double>() {
                        @Override
                        public double applyAsDouble(Double value) {
                            return value;
                        }
                    })
                            .tags(Tags.of("double.size", name))
                            .description("double监控")
                            .register(registry);
                }
                if (map != null) {
                    Gauge.builder(name + ".map.size", map, new ToDoubleFunction<Map>() {
                        @Override
                        public double applyAsDouble(Map value) {
                            return map.size();
                        }
                    })
                            .tags(Tags.of("map.size", name))
                            .description("Map监控")
                            .register(registry);
                }
                if (list != null) {
                    Gauge.builder(name + ".list.size", list, new ToDoubleFunction<List>() {
                        @Override
                        public double applyAsDouble(List value) {
                            return value.size();
                        }
                    })
                            .tags(Tags.of("list.size", name))
                            .description("List监控")
                            .register(registry);
                }
            } else if (registry instanceof PrometheusMeterRegistry) {
                if (value != null) {
                    Gauge.builder("jdkobject.cache.size", value, new ToDoubleFunction<Double>() {
                        @Override
                        public double applyAsDouble(Double value) {
                            return value;
                        }
                    })
                            .tags(Tags.of("cache.name", name))
                            .description("double监控")
                            .register(registry);
                }
                if (map != null) {
                    Gauge.builder("jdkobject.cache.size", map, new ToDoubleFunction<Map>() {
                        @Override
                        public double applyAsDouble(Map value) {
                            return map.size();
                        }
                    })
                            .tags(Tags.of("cache.name", name))
                            .description("Map监控")
                            .register(registry);
                }
                if (list != null) {
                    Gauge.builder("jdkobject.cache.size", list, new ToDoubleFunction<List>() {
                        @Override
                        public double applyAsDouble(List value) {
                            return value.size();
                        }
                    })
                            .tags(Tags.of("cache.name", name))
                            .description("List监控")
                            .register(registry);
                }
            }

        }
    }


}

