# MonitorMeter

## 1.介绍

  主要是监控系统可量化指标，目前每10S打印一次，目前支持日志,Prometheus。

  + 默认使用日志的方式，日志级别为 INFO, 修改配置:```cn.com.ry.framework.monitormeter.instrument.linklog=INFO ```
  
  + 切换Prometheus的方式,在配置文件中添加如下配置：``` monitormeter.type=prometheus ```,请求方式http://XXX:1108/prometheus

####  linkLog格式如下：

```
    {
       
        # 接口监控 
    	"timed.test.throughput": 0.1,
        "timed.test.max": 16.0432513,
    	"timed.test.mean": 16.0432513,
        
        # 表数据量监控
        "db.dataSource.area.size.value": 1.0,
        "db.dataSource.dept.size.value": 2.0,
        
        #mq监控
    	"mq.rocketmq.rocketmq1.topic.consumer.tps.value": 0.0,
    	"mq.rocketmq.rocketmq1.topic.consumer.tps.value": 0.0,
        
        #连接池监控
        "jdbc.connections.druid.dataSource.max.value": 50.0,
    	"jdbc.connections.druid.dataSource.active.value": 1.0,
    	"jdbc.connections.druid.dataSource.min.value": 5.0,
       
        #httpclient连接池监控
       	"httpclient.gateway.pending.value": 0.0,
    	"httpclient.gateway.available.value": 0.0,
    	"httpclient.gateway.leased.value": 0.0,
    	"httpclient.gateway.max.value": 1000.0,
       
        #缓存监控
        "cache.map.size.value":100.0,
        "cache.list.size.value":100.0,
        "cache.double.size.value":100.0
    }
```
    
#### Prometheus格式如下

```
# 数据库表数据量监控 k= db_query_size_rows
# datasource_name =数据源的名称  table_name为表名称
db_query_size_rows{datasource_name="dataSource",table_name="area",} 1.0
db_query_size_rows{datasource_name="dataSource",table_name="dept",} 219.0


# 连接池监控  jdbc_connections_min=初始连接数  jdbc_connections_active=当前占用连接数 jdbc_connections_max=最大连接数
# datasource_name=数据源名称
jdbc_connections_min{datasource_name="dataSource",} 5.0
jdbc_connections_active{datasource_name="dataSource",} 1.0
jdbc_connections_max{datasource_name="dataSource",} 50.0

# 方法监控 一个采集周期内  method_timed_seconds_count=请求次数  method_timed_seconds_sum=请求总耗时 method_timed_seconds_max=最大耗时
# class=类名 method=方法名
method_timed_seconds_count{class="cn.com.ry.framework.demo.controller.TestController",exception="none",method="test",} 1.0
method_timed_seconds_sum{class="cn.com.ry.framework.demo.controller.TestController",exception="none",method="test",} 16.4556164
method_timed_seconds_max{class="cn.com.ry.framework.demo.controller.TestController",exception="none",method="test",} 16.4556164

# mq监控 mq_rocketmq_tps TPS监控，单位 数量/秒。 mq_rocketmq_totalDiff 积压量
# mq_consumerGroup=消费组  mq_topic=topic mq_type=mq类型
mq_rocketmq_tps{mq_consumerGroup="consumerGroup",mq_topic="topic",mq_type="rocketMq",} 0.0
mq_rocketmq_totalDiff{mq_consumerGroup="consumerGroup",mq_topic="topic",mq_type="rocketMq",} 0.0

# httpclient连接池监控 httpclient_pool_leased=当前使用数量 httpclient_pool_pending=准备使用数量  httpclient_pool_max=连接池最大数量  httpclient_pool_available=连接池中可用的连接数，这里的是连接被使用完后可被复用
httpclient_pool_pending{httpclient_pool_name="gateway",} 0.0
httpclient_pool_leased{httpclient_pool_name="gateway",} 0.0
httpclient_pool_max{httpclient_pool_name="gateway",} 1000.0
httpclient_pool_available{httpclient_pool_name="gateway",} 0.0

# HELP jdkobject_cache_size 缓存监控
# cache_name 缓存类型
jdkobject_cache_size{cache_name="dubleCache",} 10.0
jdkobject_cache_size{cache_name="listCahce",} 100.0
jdkobject_cache_size{cache_name="mapCache",} 100.0



```

## 2.引入MonitorMeter

+ pom.xml中添加依赖

```
   <dependency>
            <groupId>cn.com.ry.framewrok</groupId>
            <artifactId>ry-monitormeter</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency>

```

## 3.监控使用方式

### 3.1接口及方法监控

给方法添加注解```@Timed(value = "timed.test")```,value为指标的名称
如果使用Prometheus的方式的方式就不要写value,直接使用``` @Timed ```

如下,就可以监控接口的性能指标。

```
    @RequestMapping("/")
    @ResponseBody
    @Timed(value = "timed.test")
    // @Timed Prometheus的方式
    public String test() throws IOException, InterruptedException {
        Thread.sleep(new Random().nextInt(3000));
        return "成功";
    }


```

+ "timed.test.throughput": 0.1   当前TPS 
+ "timed.test.max": 16.0432513,  最大请求时长
+ "timed.test.mean": 16.0432513, 10S内平均每次请求时长 

### 3.2数据库连接池监控  

数据库连接池自动发现，目前支持DBCP,Druid,Hikari  

+ "jdbc.connections.druid.dataSource.max.value": 50.0, 最大连接池
+ "jdbc.connections.druid.dataSource.active.value": 1.0, 当前使用连接数
+ "jdbc.connections.druid.dataSource.min.value": 5.0, 空闲开启最小连接数
       

### 3.3自定义监控

DEMO如下：
```
@Service
public class MonitorAutoConfiguration implements InitializingBean {

    //注册器
    @Autowired
    MeterRegistry meterRegistry;
    //数据源
    @Autowired
    DataSource dataSource;


    @Override
    public void afterPropertiesSet() throws Exception {
        //RocketMQ监控
        String name = "rocketmq1";
        String nameserver = "10.206.36.22:9876";
        new MQMonitor(name, nameserver)
                .topic(""CancelWaybill"", "CancelWaybill_RCB_MS")
                .topic("SignConfirmMQ", "SignConfirm_RCB_MS")
                .buildMqMetrics(meterRegistry);


        //数据库表监控
        new JDBCMonitor("dataSource", dataSource)
                .query("account_area_info", "select count(1) from account_area_info")
                .query("account_dept_area", "select count(1) from account_dept_area")
                .buildQueryMetrics(meterRegistry);


        //缓存监控
        Map map = new HashMap();
        for (int i = 0; i < 100; i++) {
            map.put("data" + i, i);
        }
        new JavaCacheMetrics(map, "cache").bindTo(meterRegistry);

        List list = new ArrayList();
        for (int i = 0; i < 100; i++) {
            list.add(i);
        }
        new JavaCacheMetrics(list, "cahce").bindTo(meterRegistry);
        
        new JavaCacheMetrics(10.0, "cahce").bindTo(meterRegistry);
		
		//httpclient监控
		int maxTotal=1000;
		int maxPerRoute=50;
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(maxTotal);
        cm.setDefaultMaxPerRoute(maxPerRoute);
        new HttpClientPoolMetric("gateway", cm).bindTo(meterRegistry);

    }
}
```

#### 3.3.1 表数据量监控


+ "db.dataSource.account_area_info.size.value": 1.0, 名称为account_area_info对应的表数据数量
+ "db.dataSource.account_dept_area.size.value": 219.0, 名称为account_dept_area对应的表数据数量
        

#### 3.3.2 RocketMq监控

+ "mq.rocketmq.rocketmq1.CancelWaybill.CancelWaybill_RCB_MS.tps.value": 0.0,   topic为CancelWaybill，消费组为CancelWaybill_RCB_MS的tps
+ "mq.rocketmq.rocketmq1.CancelWaybill.CancelWaybill_RCB_MS.totalDiff.value": 0.0,    topic为CancelWaybill，消费组为CancelWaybill_RCB_MS的积压量
+ "mq.rocketmq.rocketmq1.SignConfirmMQ.SignConfirm_RCB_MS.tps.value": 0.0,   topic为SignConfirmMQ，消费组为SignConfirm_RCB_MS的tps
+ "mq.rocketmq.rocketmq1.SignConfirmMQ.SignConfirm_RCB_MS.totalDiff.value": 0.0,   topic为SignConfirmMQ，消费组为SignConfirm_RCB_MS的积压量
  
#### 3.3.3 缓存监控

+ "cache.map.size.value":100.0, map的size大小
+ "cache.list.size.value":100.0 list的size大小
+ "cache.double.size.value":100.0 double的数值

#### 3.3.4 httpclient连接池监控

+ "httpclient.gateway.pending.value": 0.0,  等待获取连接的线程数
+ "httpclient.gateway.available.value": 0.0, 连接池中可用的连接数，这里的是连接被使用完后可被复用
+ "httpclient.gateway.leased.value": 0.0,   连接池中正在使用的连接数
+ "httpclient.gateway.max.value": 1000.0, 连接池最大可连接数


